#include<stdio.h>
#include<math.h>
void main()
{
float a,b,c,d,r1,r2,re,im;
printf("Enter the co-efficients of the quadratic equation\n");
scanf("%f%f%f",&a,&b,&c);
d=(b*b)-(4*a*c);
if(d>0)
{
r1=(-b+sqrt(d))/(2*a);
r2=(-b-sqrt(d))/(2*a);
printf("roots are real\n");
printf("First root: %f",r1);
printf("Second root: %f",r2);
}
else if(d==0)
{
r1=r2=-b/(2*a);
printf("Roots are real and equal\n");
printf("First root: %f",r1);
printf("Second root: %f",r2);
}
else if(d<0)
{
re=-b/(2*a);
im=sqrt(-d)/(2*a);
printf("Roots are imaginary\n");
printf("First root: %f+i%f",re,im);
printf("Second root: %f-i%f",re,im);
}
}


