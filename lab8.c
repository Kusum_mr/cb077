#include <stdio.h>

int main()
{
    struct student
    {
        char name[15];
        int rollno;
        char sect[2];
        float fees;
        char dep[15];
        int result;
    };
    struct student s1,s2;
    
    printf("Enter the first student details in the order:\n");
    
    printf("Enter the name:\n");
    scanf("%s",s1.name);
    printf("Enter the roll number:\n");
    scanf("%d",&s1.rollno);
    printf("Enter the section:\n");
    scanf("%s",s1.sect);
    printf("Enter the fees:\n");
    scanf("%f",&s1.fees);
    printf("Enter the department:\n");
    scanf("%s",s1.dep);
    printf("Enter result:\n");
    scanf("%d",&s1.result);
    
    printf("Enter the second student details in the order:\n");
    
    printf("Enter the name:\n");
    scanf("%s",s2.name);
    printf("Enter the roll number:\n");
    scanf("%d",&s2.rollno);
    printf("Enter the section:\n");
    scanf("%s",s2.sect);
    printf("Enter the fees:\n");
    scanf("%f",&s2.fees);
    printf("Enter the department:\n");
    scanf("%s",s2.dep);
    printf("Enter result:\n");
    scanf("%d",&s2.result);
    
    
    
    if(s1.result>s2.result)
    printf("Details of student who has scored highest:\n%s\n%d\n%s\n%f\n%s\n%d\n",s1.name,s1.rollno,s1.sect,s1.fees,s1.dep,s1.result);
    else
    printf("Details of student who has scored highest:\n%s\n%d\n%s\n%f\n%s\n%d\n",s2.name,s2.rollno,s2.sect,s2.fees,s2.dep,s2.result);

    return 0;
}