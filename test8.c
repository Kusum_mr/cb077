#include <stdio.h>

int main()
{
    struct employee
    {
        int id;
        char name[15];
        float sal;
    };
    struct employee e;
    
    printf("Enter the name of the employee\n");
    scanf("%s",e.name);
    printf("Enter the employee ID\n");
    scanf("%d",&e.id);
    printf("Enter the salary of the employee\n");
    scanf("%f",&e.sal);
    
    printf("DETAILS OF THE EMPLOYEE ARE\n");
    printf("%s\n %d\n %f\n",e.name,e.id,e.sal);
    
    return 0;
    
}