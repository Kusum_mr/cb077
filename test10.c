#include<stdio.h>
int main()
{
    FILE *fp;
    char c;
    printf("DATA INPUT\n");
    fp=fopen("INPUT.txt","w");
    while((c=getchar())!=EOF)
    putc(c,fp);
    fclose(fp);
    printf("Writing to a file\n");
    printf("DATA OUTPUT\n");
    fp=fopen("INPUT.txt","r");
    while((c=getc(fp))!=EOF)
        printf("%c",c);
    fclose(fp);
    return 0;
}
