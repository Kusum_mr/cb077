#include <stdio.h>

int main()
{
    int a[10][10],m,n,i,j,temp,inx;
    printf("Enter the order of the matrix(row,column)\n");
    scanf("%d%d",&m,&n);
    printf("Enter the elements of the matrix:\n");
    for(i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        scanf("%d",&a[i][j]);
    }
    inx = m > n ? m : n;
    for(i=0;i<inx;i++)
    {
        for(j=0;j<i;j++)
        {
            temp=a[i][j];
            a[i][j]=a[j][i];
            a[j][i]=temp;
        }
    }
    printf("The matrix after transpose is:\n");
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        printf("%d ",a[i][j]);
        printf("\n");
    }

    return 0;
}
