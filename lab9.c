#include <stdio.h>

int main()
{
    float num1, num2;    
    float *ptr1, *ptr2; 

    float sum, diff, mult, divi;int rem;
    ptr1 = &num1; 
    ptr2 = &num2; 

    
    printf("Enter any two real numbers: ");
    scanf("%f%f", ptr1, ptr2);

    
    sum  = (*ptr1) + (*ptr2);
    diff = (*ptr1) - (*ptr2);
    mult = (*ptr1) * (*ptr2);
    divi = (*ptr1) / (*ptr2);
    rem = (int)(*ptr1) % (int)(*ptr2);
    
    printf("Sum = %f\n", sum);
    printf("Difference = %f\n", diff);
    printf("Product = %f\n", mult);
    printf("Quotient = %f\n", divi);
    printf("Remainder = %d\n", rem);

    return 0;
}
