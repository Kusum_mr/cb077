#include <stdio.h>

int main()
{
    int n;
    printf("Enter the order of square matrix(n*n)\n");
    scanf("%d",&n);
    int a[n][n];
    int diagonal=0,above_diagonal=0,below_diagonal=0;
    printf("Enter the elements\n");
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        scanf("%d",&a[i][j]);
    }
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
            if(i==j)
            diagonal+=a[i][j];
            else if(i<j)
            above_diagonal+=a[i][j];
            else if(i>j)
            below_diagonal+=a[i][j];
        }
        
    }
    printf("Sum of the elements along the diagonal: %d\n",diagonal);
    printf("Sum of the elements below diagonal: %d\n",below_diagonal);
    printf("Sum of the elements above diagonal: %d\n",above_diagonal);
    return 0;
}
        
