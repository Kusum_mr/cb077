#include <stdio.h>

int main()
{
    int n,i=1;
    printf("Enter the number n\n");
    scanf("%d",&n);
    if(n>=1 && n<=100)
    {
    printf("The multiples of n from 1 to 100:\n");
    while((n*i)>=1 && (n*i)<=100)
    {
        printf("%d\n",n*i);
        i++;
    }
    }
    else
    printf("Invalid input");
    return 0;
    
}